import { GraphQLClient } from 'graphql-request';
import { loader } from 'graphql.macro';

const getRoutesAST = loader('./queries/get-routes.graphql');
const getRoutePointsAST = loader('./queries/get-route-points.graphql');
const getRoutesAtPointAST = loader('./queries/get-routes-at-point.graphql');

const getRoutesQuery = getRoutesAST.loc?.source.body;
const getRoutePointsQuery = getRoutePointsAST.loc?.source.body;
const getRoutesAtPointQuery = getRoutesAtPointAST.loc?.source.body;

const {
  REACT_APP_HASURA_ENDPOINT: endpoint,
  REACT_APP_HASURA_HEADER_KEY: key,
  REACT_APP_HASURA_HEADER_VALUE: value,
} = process.env;

const graphQLClient = new GraphQLClient(endpoint as string, {
  headers: {
    [key as string]: value as string,
  }
});

const getRoutes = () => {
  if (getRoutesQuery) {
    return graphQLClient.request(getRoutesQuery);
  }

  throw new Error('No GraphQL query.');
};

const getRoutesAtPoint = (bufferedPoint: any) => {
  if (getRoutesAtPointQuery) {
    return graphQLClient.request(getRoutesAtPointQuery, { buffer: bufferedPoint });
  }

  throw new Error('No GraphQL query.');
};

const getRoutePoints = (id: string) => {
  if (getRoutePointsQuery) {
    return graphQLClient.request(getRoutePointsQuery, { routeId: id });
  }

  throw new Error('No GraphQL query.');
};

export default {
  getRoutes,
  getRoutePoints,
  getRoutesAtPoint,
}
