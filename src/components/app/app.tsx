import React, { useEffect, useState } from 'react';

import MyMap from 'components/my-map/my-map';
import Menu, { MenuItem } from 'components/menu/menu';
import graphql from 'services/graphql';

import styles from './app.module.scss';

interface AppState {
  routes: MenuItem[],
  currentRoute: string|null,
}

const defaultAppState: AppState = {
  routes: [],
  currentRoute: null,
};

const App: React.FC = () => {
  const [ appState, setAppState ] = useState(defaultAppState);
  useEffect(() => {
    graphql.getRoutes().then(data => {
      const { routes } = data;
      setAppState(state => ({
        ...state,
        routes,
      }));
    });

  }, []);

  const setRoute = (id: string) => setAppState({
    ...appState,
    currentRoute: id,
  });

  const highlight = (items: MenuItem[]) => {
    const ids = items.map(it => it.id);

    setAppState({
      ...appState,
      routes: routes.map(route => ({
        ...route,
        highlighted: ids.includes(route.id),
      })),
    });
  };

  const { currentRoute, routes } = appState;
  const menuProps = { routes, setRoute };
  const myMapProps = { currentRoute, highlight };

  return (
    <div className={styles.App}>
      <Menu {...menuProps} />
      <MyMap {...myMapProps} />
    </div>
  );
}

export default App;
