import React, { useEffect, useState } from 'react';
import ReactMapGL, { ViewState, PointerEvent } from 'react-map-gl';
import { buffer, point } from '@turf/turf';

import graphql from 'services/graphql';
import { MenuItem } from 'components/menu/menu';
import defaultMapStyle from './default-map-style.json';

interface MyMapProps {
  currentRoute: string|null,
  highlight: (routes: MenuItem[]) => void,
}

const { REACT_APP_MAPBOX_TOKEN: MapboxToken } = process.env;
const initialMapState: ViewState = {
  zoom: 12.5,
  latitude: 41.13950680312654,
  longitude: -8.610252444771186,
  bearing: -38,
  pitch: 60
};
const initialRoutes: any = {};

const MyMap: React.FC<MyMapProps> = ({ currentRoute, highlight }: MyMapProps) => {
  const [mapState, setMapState] = useState(initialMapState);
  const [routes, setRoutes] = useState(initialRoutes);
  useEffect(() => {
    if (currentRoute) {
      if(!routes[currentRoute]) {
        graphql.getRoutePoints(currentRoute).then(data => {
          const { points: rawPoints } = data;
          const points = rawPoints.map((rawPt: any) => rawPt.point.coordinates);
          setRoutes({
            ...routes,
            [currentRoute]: points,
          });
        });
      }
    }
  }, [currentRoute, routes]);
  const viewportChangeHandler = (viewport: ViewState) => setMapState({...viewport});
  const clickHandler = ({lngLat}: PointerEvent) => {
    const bufferedPoint = buffer(point(lngLat), 0.03, {units: 'kilometers'})

    graphql.getRoutesAtPoint(bufferedPoint.geometry).then(data => {
      highlight(data.routes);
    });
  };
  const coordinates = currentRoute ? routes[currentRoute] : [];

  defaultMapStyle.sources = {
    ...defaultMapStyle.sources,
    routes: {
      type: 'geojson',
      data: {
        type: "Feature",
        properties: { },
        geometry: {
          type: "LineString",
          coordinates: coordinates || [],
        }
      }
    },
  } as any;

  return (
    <div style={{width: '100vw', height: '100vh'}}>
      <ReactMapGL
        {...mapState}
        width="100%"
        height="100%"
        mapboxApiAccessToken={MapboxToken}
        mapStyle={{...defaultMapStyle}}
        onViewportChange={viewportChangeHandler}
        onClick={clickHandler}
      />
    </div>
  );
}

export default MyMap;
