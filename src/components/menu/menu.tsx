import React from 'react';

import styles from './menu.module.scss';

type ClickHandler = (id: string) => void;

export interface MenuItem {
  id: string,
  name: string,
  highlighted?: boolean,
}

interface MenuProps {
  routes: MenuItem[],
  setRoute: ClickHandler,
}

const highlightedIcon = (highlighted: boolean) => highlighted ? '✔️' : '';
const itemText = ({ name, highlighted }: MenuItem) => `🦝 ${name} ${highlightedIcon(highlighted || false)}`;

const createItems = (items: MenuItem[], setRoute: ClickHandler) => items.map((item: MenuItem) => (
  <li key={item.id} onClick={() => setRoute(item.id)}>{ itemText(item) }</li>
));

const Menu: React.FC<MenuProps> = ({ routes, setRoute }: MenuProps) => (
  <div className={styles.MenuContainer}>
    <h3>Routes</h3>
    <ul>
      { createItems(routes, setRoute) }
    </ul>
  </div>
);

export default Menu;
