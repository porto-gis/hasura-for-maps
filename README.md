🦝 Use [Hasura](https://hasura.io/) for maps 🦝

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

Small demo of how easy is to create map applications with [Hasura](https://hasura.io/). A post coming soon!

![tables](./docs/images/hasuraMap001.png)

![points](./docs/images/hasuraMap002.png)

![routes](./docs/images/hasuraMap003.png)

Check `docker-compose.yaml` for Hasura config. Docker images used:
- PostGIS(postgis/postgis:12-3.0-alpine)
- Hasura(hasura/graphql-engine:v1.1.1)
- Caddy(abiosoft/caddy:0.11.0)